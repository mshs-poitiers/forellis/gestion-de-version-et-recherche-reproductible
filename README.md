# Gestion de version et recherche reproductible

Conférence autour de la recherche reproductible et la gestion de version.

## La presentation 

<a href="https://gitlab.huma-num.fr/mshs-poitiers/forellis/gestion-de-version-et-recherche-reproductible/-/blob/master/Titre_final_v3-correctionOK_0.2.md"><img src="imgs/presentation_printscreen.jpg" width="300" alt="Flower"></a>

[Afficher la presentation](https://gitlab.huma-num.fr/mshs-poitiers/forellis/gestion-de-version-et-recherche-reproductible/-/blob/master/Titre_final_v3-correctionOK_0.2.md)



**Notes**

Cette présentation a été écrite en markdown avec [https://stackedit.io/app#](stackedit), sauvergardé en .MD dans le fichier [Titre_final_v3-correctionOK_0.2.md](Titre_final_v3-correctionOK_0.2.md), 
puis exporté en html avec TOC (table of content) dans le fichier [public/Titre_final_v3-correctionOK_0.2.html](public/Titre_final_v3-correctionOK_0.2.html).

Le rendu du html est réalisé par l'intermédiaire du service [https://htmlpreview.github.io/](https://htmlpreview.github.io/).

## Compléments à lire
[Dossier](sources/) 