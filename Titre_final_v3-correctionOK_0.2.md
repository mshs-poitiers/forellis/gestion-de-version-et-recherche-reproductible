﻿# Titre_final_v3-correctionOK_0.2 
## Gestion de version et recherche reproductible



<b>Michael Nauge</b>$^{1,2}$
 Ingénieur Corpus Numérique

[[1] : FORELLIS, EA3816, Université de Poitiers](http://forellis.labo.univ-poitiers.fr/)

[[2] : MIMMOC, EA3812, Université de Poitiers](http://mimmoc.labo.univ-poitiers.fr/)
 
 ---

![Corpus numérique](https://cdn.pixabay.com/photo/2017/09/16/19/21/salad-2756467_960_720.jpg)
<i>Image par <a href="https://pixabay.com/fr/users/silviarita-3142410/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2756467">silviarita</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2756467">Pixabay</a></i>

## Corpus : définition
* Dictionnaire Le Trésor de la Langue Française : "Ensemble de textes établi selon un principe de documentation exhaustive, un critère thématique ou exemplaire en vue de leur étude linguistique"

* John Sinclair, un linguiste britannique : "Une collection de données langagières sélectionnées et organisées selon des critères linguistiques explicites pour servir d'échantillon du langage"

* "Agrégation de sources cohérentes construite pour répondre à une question scientifique"

*  "Agrégation de sources <i>(imparfaites) </i>représentatives  <i>(d’un ensemble plus grand 
 indénombrable)</i> pour répondre à une question scientifique <i>(parfois difficile à poser)</i>"

### Corpus vivant
<a href="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth"><img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_GenericProject.svg"></a>




## 1 - Tranche de vie

### 1.1 - USB.K iLLER

![illustration usb-killer](https://cdn.pixabay.com/photo/2013/07/13/12/38/deaths-head-160033_960_720.png)

<i>Image par <a href="https://pixabay.com/fr/users/OpenClipart-Vectors-30363/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=160033">OpenClipart-Vectors</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=160033">Pixabay</a></i>

#### Marie Golpas, Étudiante en master 1 :
> "Je voulais faire une copie de sauvegarde sur ma clé, j'ai finalement perdu mon travail de la semaine", 

* Cause : confusion source/destination

#### Roger Touperdu, Doctorant :
> "Je travail directement sur ma clé usb car j'utilise beaucoup de machines différentes", 

* Cause : Nombre d'écriture Max (3000 écritures). MLC (multi-level cell) Mémoire intégrée dans la plupart des clés USB.
 


### 1.2 - Ça ne marche plus
> J’ai récemment participé à la rédaction d’un article scientifique et nous venons de recevoir les commentaires des relecteurs : je dois modifier les couleurs d’une figure afin que celle-ci soit lisible en noir et blanc. Comme je n’arrivais pas à remettre la main sur mon script R ayant généré la figure en question, j’ai ré-écrit le programme correspondant. Le seul problème, c’est que cette nouvelle figure est un peu différente de la précédente et remet en cause les conclusions de l’article. Je ne comprends pas ce qui a pu se passer. 


[source : Vers une recherche reproductible](https://rr-france.github.io/bookrr/A-personas.html)


## 2 - Causes des problèmes

* Besoin d'ubiquité des données
* Besoin de garder les traces des modifications des données
* Besoin de pouvoir rejouer les opérations de manipulation des données

## 3 - Gestion des fichiers 
### 3.1 - Convention de nommage
**Cas A : gestion des versions**
version précédente 1 : 2019-02-03_analyse_donnees_v00.R
version précédente 2 : 2019-04-17_analyse_donnees_v01.R
version courante :  analyse_donnees.R

**Cas B :  gestion des métadonnées**
MS_ENS04-L_HAM-G_2017-2018_B.txt
MS_ENS01-A_LEO-F_2017-2018_B.txt


| section | idEns | idEleve | genre | anneeScol | protocol|
|--|--|--|--|--|--|
| MS | ENS04 | L_HAM | G | 2017-2018 | B |
| MS | ENS01 | A_LEO | F | 2017-2018 | B |



### 3.2 - Versionner avec GIT
### 3.2.1 - Back to the future
![image voyage dans le temps](https://cdn.pixabay.com/photo/2017/10/07/09/12/time-2825832_960_720.png)
<i>Image par <a href="https://pixabay.com/fr/users/geralt-9301/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2825832">Gerd Altmann</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2825832">Pixabay</a></i>


Imaginez un monde où vous pourriez voyager dans l'historique de toutes les créations/suppressions/renommages/modifications de tous vos fichiers/dossiers depuis le début de votre thèse...

#### 3.2.2 - Un monde d'instantané discret

![illustration snapshot](https://git-scm.com/book/en/v2/images/snapshots.png)
<i>Image de <a href="https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Rudiments-de-Git">git-scm</a></i>

Vous devez valider  des  instantanés  de votre "dépôt/**repository**" chaque fois que votre projet atteint un état que vous souhaitez enregistrer. 

    commit

> Tout est géré localement, mais nous avons droit à des bouées de secours distantes :-)

![illustrationWorkflow git remote](https://www.git-tower.com/learn/media/pages/git/ebook/en/command-line/remote-repositories/introduction/-1045933932-1566804922/basic-remote-workflow.png)

Pour mettre à jour un dépôt distant (**remote repository/origin**) 

    push

Pour rapatrier un projet sur une nouvelle machine

    clone
Pour rapatrier les dernières modifications

    pull 
    ou
    fetch+merge


<i>Image de <a href="https://www.git-tower.com/learn/git/ebook/en/command-line/remote-repositories/introduction">git-tower</a></i>

#### 3.2.3 - Sites d’hébergement de "remote repository"
[Framagit](https://framagit.org/public/projects)
[Gitlab Huma-Num](https://gitlab.huma-num.fr/explore/projects)
[Github](https://github.com/explore)

#### 3.2.4 - Pour aller plus loin
[documentation officielle en français](https://git-scm.com/book/fr/v2/Les-bases-de-Git-D%C3%A9marrer-un-d%C3%A9p%C3%B4t-Git)
[openclassrooms : gerez votre code avec git](https://openclassrooms.com/fr/courses/2342361-gerez-votre-code-avec-git-et-github)


## 4 - Documenter et Rejouer la chaîne des opérations
>Comment conserver des explications détaillées de sa démarche scientifique en y associant des exécutions de code de création, de manipulation et de visualisation de ses données ?

[MOOC - Recherche reproductible : principes méthodologiques pour une science transparente ](https://www.fun-mooc.fr/courses/course-v1:inria+41016+session01bis/about)

### 4.1 Document computationnel/Notebooks
![illustration notebook](https://upload.wikimedia.org/wikipedia/commons/a/af/IPython-notebook.png)

Solutions actuelles : 
* [jupyter](https://jupyter.org/)
* [r notebook](https://bookdown.org/yihui/rmarkdown/notebook.html)
* [org-mode](https://www.orgmode.org/)
* [Iodide](https://alpha.iodide.io/)

### 4.2 Démonstration interactive sur un corpus en SHS
[Lancer la démo](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mshs-poitiers/forellis/marmython/raw/master/notebook/marmython.ipynb?flush_cache=true) 
[Lancer la démo. intéractive](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmshs-poitiers%2Fforellis%2Fmarmython/16bf1829efb37cee925155b2e7c953113c67dc37?filepath=notebook%2Fmarmython.ipynb) (merci de patienter quelques minutes, une instance de machine virtuelle va s’installer et se lancer rien que pour nous)

## 5 - Faciliter la réappropriation
> Comment minimiser le temps d'appropriation d'un jeu de données ?

### 5.1 -  Frictionless data
Vidéo explicative (≈ 7 minutes) :
<a href="https://www.youtube.com/watch?v=lWHKVXxuci0" target="_blank"><img src="http://img.youtube.com/vi/lWHKVXxuci0/0.jpg" 
alt="miniature video friction less data"/></a>

* Structurer ses données en [data package](http://frictionlessdata.io/data-packages/) 
* Vérifier l'intégrité des données avec [table-schema et goodtables](https://frictionlessdata.io/docs/table-schema/) 

### 5.2 - Goodtables
Document explicatif (≈ 20 minutes de lecture) 
[Partage d'expérience de nos premiers pas avec goodtables](https://gitlab.huma-num.fr/mnauge/goodtablesexperiment)

## Conclusion
![illustration conclusion zen](https://cdn.pixabay.com/photo/2015/04/06/19/40/fish-709886_960_720.jpg)
Image par <a href="https://pixabay.com/fr/users/monstreh-637659/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=709886">Анна Куликова</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=709886">Pixabay</a>

**La recherche reproductible** :
* C'est un état d'esprit
	* Structurant 
		* convention de nommage
		* spécification pour l'intégrité des données
		* rationalisation des chaînes de traitement
		* **poser par écrit et de manière formelle**
	* Rassurant
		* minimiser le risque de perte de données et donc de temps 
		* gagner en crédibilité
		
* Des outils logiciels
	* Gestionnaire de version des fichiers
	*  Documents computationnels














